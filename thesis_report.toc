\contentsline {chapter}{Abstract}{i}{section*.1}
\contentsline {chapter}{Zusammenfassung}{ii}{section*.3}
\contentsline {chapter}{Acknowledgments}{iii}{section*.5}
\contentsline {chapter}{Contents}{iv}{section*.7}
\contentsline {chapter}{List of Figures}{vi}{section*.9}
\contentsline {chapter}{List of Tables}{viii}{section*.11}
\contentsline {chapter}{List of Abbreviations}{ix}{section*.13}
\contentsline {chapter}{\numberline {1}Introduction }{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Objective}{3}{section.1.2}
\contentsline {chapter}{\numberline {2}Background }{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Spatial Geometry Model }{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}GML Linear Ring }{6}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}GML Polygon }{7}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Problem Definitions }{9}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Errors in 3D Building Model }{9}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Planarity Error }{10}{subsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.1}DISTANCE\_PLANE Error}{11}{subsubsection.2.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2.2}NORMALS\_DEVIATION Error}{12}{subsubsection.2.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.3}Floating Point Number Error}{13}{subsubsection.2.2.2.3}
\contentsline {chapter}{\numberline {3}Related Work }{15}{chapter.3}
\contentsline {chapter}{\numberline {4}Validation and Automatic Repair Methodology }{21}{chapter.4}
\contentsline {section}{\numberline {4.1}Validity Criteria }{22}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Tolerances }{22}{subsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.1}Tolerance for DISTANCE\_PLANE Error}{22}{subsubsection.4.1.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.2}Tolerance for NORMALS\_DEVIATION Error}{22}{subsubsection.4.1.1.2}
\contentsline {subsubsection}{\numberline {4.1.1.3}Snapping Tolerance }{23}{subsubsection.4.1.1.3}
\contentsline {subsection}{\numberline {4.1.2}Error Dependencies }{23}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Halfedge Data Structure }{23}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Planarity Error Validation }{24}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Validation Rules }{24}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Distance between Two Vertices}{25}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Least-squares Plane-fitting }{25}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Distance between a Point and the Adjusted Plane }{26}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}Repair Methodology }{26}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Repair Folds in the Polygon }{27}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Repair Floating Point Number Error }{28}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Repair Planarity Error }{31}{subsection.4.3.3}
\contentsline {chapter}{\numberline {5}Prototype Implementation }{34}{chapter.5}
\contentsline {section}{\numberline {5.1}Development Frameworks }{34}{section.5.1}
\contentsline {section}{\numberline {5.2}Workflow }{35}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Read CityGML Model }{36}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}CityGML to Polyhedral Surface }{36}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Check Folds in the Polygon }{36}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Repair Folds in the Polygon }{37}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Check Floating Point Number Error }{38}{subsection.5.2.5}
\contentsline {subsection}{\numberline {5.2.6}Repair Floating Point Number Error }{39}{subsection.5.2.6}
\contentsline {subsection}{\numberline {5.2.7}Planarity Check }{39}{subsection.5.2.7}
\contentsline {subsection}{\numberline {5.2.8}Healing of Planarity Error }{40}{subsection.5.2.8}
\contentsline {subsection}{\numberline {5.2.9}Generate Planarized CityGML Model }{42}{subsection.5.2.9}
\contentsline {subsection}{\numberline {5.2.10}Test Report }{42}{subsection.5.2.10}
\contentsline {chapter}{\numberline {6}Experimental Results and Discussion }{43}{chapter.6}
\contentsline {section}{\numberline {6.1}Experimental Results }{43}{section.6.1}
\contentsline {section}{\numberline {6.2}Problems and Discussion }{50}{section.6.2}
\contentsline {chapter}{\numberline {7}Conclusions and Future Work }{55}{chapter.7}
\contentsline {chapter}{Bibliography}{57}{section*.59}
\contentsline {chapter}{\numberline {A}Example Polygons}{x}{appendix.A}
\contentsline {section}{\numberline {A.1}Non-planar Polygon }{x}{section.A.1}
\contentsline {section}{\numberline {A.2}Repaired Polygons }{xi}{section.A.2}
\contentsline {chapter}{\numberline {B}CD}{xii}{appendix.B}
\contentsline {section}{\numberline {B.1}Digital Version of the Master Thesis}{xii}{section.B.1}
\contentsline {section}{\numberline {B.2}Programming Implementations and Test Datasets}{xii}{section.B.2}
\contentsline {chapter}{Declaration of Authorship}{xiii}{section*.61}
