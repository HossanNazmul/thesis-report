\chapter{Background \label{cha:chapter2}}
This chapter describes the valid geometric characteristics of 3D geometric primitives, polygons as well as an overview of common errors in 3D building models. Different aspects of planarity error are also explained in this chapter.

\section{Spatial Geometry Model \label{sec:sota}}
The spatial characteristics of a valid geometry are outlined in ISO 19107 spatial schema. Geography Markup Language (GML) \citep{2007_ogc_GML} and CityGML \citep{2012_ogc_CityGML} are the realization of ISO 19107 spatial schema and an Open Geospatial Consortium (OGC) standard. The geometries in GML and CityGML are represented according to the Boundary Representation (B-Rep). CityGML uses only a subset of GML3 geometry package \citep{2012_ogc_CityGML} for the representation of virtual 3D city models \citep{2009_Kolbe}. Besides geometric representation, CityGML can preserve the semantic and thematic features of the city model. OGC Quality Interoperability Experiment (QIE) \citep{2016_ogc_qie} presented the data quality specification, 3D model implementation guidance and quality checking to assure the validity of CityGML data. 

According to the ISO 19107 \citep{2003_ISO19107}, the geometric primitives are:
\begin{itemize}[leftmargin=*]
	\item 0D primitive: GM\_Point.
	\item 1D primitive: GM\_Curve.
	\item 2D primitive: GM\_Surface.
	\item 3D primitive: GM\_Solid.
\end{itemize}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.70\textwidth]{./files/ISOPrimitives.JPG}
	\caption[ISO 19107 geometric primitives]{ISO 19107 geometric primitives \citep{2016_ogc_qie}.}
	\label{fig:ISOPrim}
\end{figure}

A point in space represents the vertices. A straight line segment between two vertices defines an edge. Facets consist of at least three or more edges. An object is represented either by a single geometric primitive or by a collection of primitives. In CityGML, surfaces are represented by polygons. The polygon is defined by one or more linear rings. The linear ring is the boundary of every polygon and is the fundamental element to define a 3D geometry. Based on \citep{2012_ogc_CityGML}, a building geometry is represented differently for different types of Levels of Detail (LOD). For example, 3D buildings in CityGML LOD2 are decomposed as multi-surface, composite-surface, solid or multi-solid geometry.

Planarity error occurs at the polygon level; therefore, linear rings and polygons are the main focus. The next section explains the validity constraints of GML linear rings and polygons.

\subsection{GML Linear Ring \label{sec:Lrings}}
According to \citep{2014_SIG3D}\footnote{Special Interest Group 3D (SIG3D) -- http://en.wiki.quality.sig3d.org}, a finite sequence of points describe a linear ring and is valid if the following conditions are fulfilled:

(1) The linear ring must have at least 4 points.\\
(2) The first and the last point represents the same point.\\
(3) All points (except first and the last) of a linear ring are different.\\
(4) Intersection of two edges can only be in one start and end point. No other intersection is allowed.\\
(5) A linear ring must be planar (e.g. all points defining a linear ring must lie on the same plane). However, a small tolerance is allowed.

The figure \ref{fig:LinearRing} illustrates some examples of linear rings in which only four points (P1, P2, P3, P4) define the boundary of the linear ring and are assumed to be on the same plane (except figure \ref{fig:LinearRing} (d)). The order of the point sequences is important in figure \ref{fig:LinearRing} and in the list below:
\begin{itemize}[leftmargin=*]
	\item  Figure \ref{fig:LinearRing} (a): The point sequences for this linear ring are (P1, P2, P3, P4, P1) which makes the linear ring closed. It fulfills all the aforementioned requirements and is valid.
	\item  Figure \ref{fig:LinearRing} (b): The point sequences are (P1, P2, P3, P4) and the linear ring is not closed, so it is invalid.
	\item  Figure \ref{fig:LinearRing} (c): (P1, P3, P4, P2, P1) are the point sequences in which the order is wrong and caused an intersection; thus, the linear ring is invalid.
	\item  Figure \ref{fig:LinearRing} (d): The point sequences are (P1, P2, P3, P4, P5, P1) in which all requirements of a linear ring are fulfilled except planarity. Point P4 is not on the plane (exceeds predefined tolerance); therefore, the linear ring is invalid.
\end{itemize}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.80\textwidth]{./files/LinearRing.pdf}
	\caption[Examples of valid and invalid linear rings]{(a) Valid linear ring (b,c,d) Invalid linear rings (adapted from: \citep{2014_SIG3D}).}
	\label{fig:LinearRing}
\end{figure}

\subsection{GML Polygon \label{sec:polygon}}
The definition of a polygon is based on \citep{2014_SIG3D}:\\
(1) A polygon is defined by the planar linear ring.\\
(2) A polygon has exactly one exterior linear ring and can have multiple interior linear rings. All exterior and interior linear rings must be on the same plane (a small tolerance is allowed).\\
(3) Interior linear rings have to be completely inside of the exterior ring.\\
(4) Interior linear rings must not overlap with other rings or be included in another linear ring (except exterior linear ring).\\
(5) Interior linear rings and the exterior linear ring may touch one another.\\
(6) The orientation of the polygon is defined by the order of the points of the exterior linear ring.

Figure \ref{fig:Polygon1} depicts some examples of valid polygons according to the GML and CityGML standard. Polygon in figure \ref{fig:Polygon1} (a) consists of one linear ring which defines the exterior. Polygons in figure \ref{fig:Polygon1} (b, c, d) consist of an exterior and one or more interior linear rings. In all cases, linear ring defining points are considered planar and all interior, exterior linear rings are on the same plane.

\begin{figure*}[h]
	\centering
	\begin{tabular}{c}
		\includegraphics[width=0.85\linewidth]{./files/valid_polygons.pdf}
	\end{tabular}
	\caption[Examples of valid polygons]{Examples of valid polygons (based on: \citep{2014_SIG3D}.}
	\label{fig:Polygon1}
\end{figure*}

In figure \ref{fig:Polygon2} (a), the area of interior linear ling is not completely inside the polygon and is invalid. The interior linear ring touches the exterior linear ring at two points. In figure \ref{fig:Polygon2} (b, d), the interiors split the polygon and therefore invalid. The interior linear ring can't include or overlap with another interior linear ring; so figure \ref{fig:Polygon2} (c) is invalid.

\begin{figure*}[h]
	\centering
	\begin{tabular}{c}
		\includegraphics[width=0.85\linewidth]{./files/invalid_polygons.pdf}
	\end{tabular}
	\caption[Examples of invalid polygons]{Examples of invalid polygons (based on: \citep{2014_SIG3D}).}
	\label{fig:Polygon2}
\end{figure*}
%\newpage
\section{Problem Definitions \label{sec:req}}
\subsection{Errors in 3D Building Model \label{sec:cmn_errors}}
Apart from planarity error, there might be other errors present in the 3D building model.  \citep{2015_N_alam_Geom_validation,2016_3dgeoinfo_citygml_errors} investigated on common errors that can be present at the different levels of geometric primitives, such as Ring, Polygon and Shell level.

Based on \citep{2016_ogc_qie}, the errors and the error codes at different geometric primitives are shortly mentioned below: 
\\
\textbf{Errors in the Ring level:} 
\begin{itemize}[leftmargin=*]
	\item \enquote{GE\_R\_TOO\_FEW\_POINTS} \\
	-- if the linear ring has less than three points
	\item \enquote{GE\_R\_CONSECUTIVE\_POINTS\_SAME}\\
	-- if the linear ring has the same consecutive point coordinates
	\item \enquote{GE\_R\_NOT\_CLOSED}\\
	-- if the first and the last point are not identical
	\item \enquote{GE\_R\_SELF\_INTERSECTION}\\
	-- if there is self-intersection in the linear ring
\end{itemize}

\textbf{Errors in the Polygon level:} 
\begin{itemize}[leftmargin=*]
	\item \enquote{GE\_P\_INTERSECTION\_RINGS}\\
	-- the intersection of two or more linear rings
	\item \enquote{GE\_P\_DUPLICATED\_RINGS}\\
	-- more than one identical linear rings
	\item \enquote{GE\_P\_NON\_PLANAR\_POLYGON\_DISTANCE\_PLANE}\\
	-- a polygon must be made of planar linear rings
	\item \enquote{GE\_P\_NON\_PLANAR\_POLYGON\_NORMALS\_DEVIATION}\\
	-- small folds on the polygon
	\item \enquote{GE\_P\_INTERIOR\_DISCONNECTED}\\
	-- polygon interiors must be connected
	\item \enquote{GE\_P\_HOLE\_OUTSIDE}\\
	-- polygon interior is located outside of the polygon
	\item \enquote{GE\_P\_INNER\_RINGS\_NESTED}\\
	-- nested interior linear rings
	\item \enquote{GE\_P\_ORIENTATION\_RINGS\_SAME}\\
	-- if the orientation of exterior and the interior linear ring is same
\end{itemize}

\textbf{Errors in the Shell level:}
\begin{itemize}[leftmargin=*]
	\item \enquote{GE\_S\_TOO\_FEW\_POLYGONS}\\
	-- a solid must be defined by at least four polygons
	\item \enquote{GE\_S\_NOT\_CLOSED}\\
	-- the solid must be a watertight object
	\item \enquote{GE\_S\_NON\_MANIFOLD\_VERTEX}\\
	-- if the incident polygons do not form one umbrella
	\item \enquote{GE\_S\_NON\_MANIFOLD\_EDGE}\\
	-- two incident polygons for each edge
	\item \enquote{GE\_S\_MULTIPLE\_CONNECTED\_COMPONENTS}\\
	-- the polygons must be connected to the shell
	\item \enquote{GE\_S\_SELF\_INTERSECTION}\\
	-- self-intersection of shells
	\item \enquote{GE\_S\_POLYGON\_WRONG\_ORIENTATION}\\
	-- wrong orientation of the single shell
	\item \enquote{GE\_S\_ALL\_POLYGONS\_WRONG\_ORIENTATON}\\
	-- all polygons of a solid have the wrong orientation
\end{itemize}

\subsection{Planarity Error \label{sec:nPlanarity}}
Non-planarity of polygons can be caused by many ways. Most of the errors occur during the generation of the 3D models \citep{2016_3dgeoinfo_citygml_errors}. The existing vendor-specific validation tools use platform specific error codes or identifiers to address the planarity error. The table \ref{tab:qie_all} is an excerpt from the OGC QIE engineering report \citep{2016_ogc_qie} in which the existing error identifiers are listed for all planarity error types. The codes are:

\begin{table}[h!]
	\begin{center}
		\caption[Vendor-specific planarity error codes]{Vendor-specific planarity error codes \citep{2016_ogc_qie}.}
		\begin{tabular}{|c|c|c|c|}
			\hline
			\textbf{QIE identifier}& \textbf{val3dity}& \textbf{CityDoctor}& \textbf{FME}\\
			\hline
			\hline
			GE\_P\_NON\_PLANAR\_POLYGON& 203& CP\_PLANNATIVE& Non-planar\\
			\_DISTANCE\_PLANE& & & surfaces\\
			\hline
			GE\_P\_NON\_PLANAR\_POLYGON& 204& CP\_PLANTRI& Non-planar\\
			\_NORMALS\_DEVIATION& & & surfaces\\
			\hline 
		\end{tabular} 
		\label{tab:qie_all}    
	\end{center}
\end{table}

This thesis addresses GE\_P\_NON\_PLANAR\_POLYGON\_DISTANCE\_PLANE error with the identifier DISTANCE\_PLANE and GE\_P\_NON\_PLANAR\_POLYGON\_NORMALS-\_DEVIATION as NORMALS\_DEVIATION error.

\subsubsection{DISTANCE\_PLANE Error}
According to \citep{2016_3dgeoinfo_citygml_errors}, DISTANCE\_PLANE error typically occurs when the perpendicular distance between a point and the fitted plane exceeds a predefined threshold limit (e.g. 1 cm). For example, if the perpendicular distance between a vertex and the fitted plane is more than the tolerance value, the polygon is identified as a non-planar polygon. The plane must be a least-squares plane. Figure \ref{fig:NonPlanarity1} illustrates an example of DISTANCE\_PLANE error situation where the perpendicular distance between the points (P1, P2, P3, and P4) and the normal vector of the plane is depicted.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{./files/DistPlane.pdf}
	\caption[Perpendicular distance from a vertex to the adjusted plane]{DISTANCE\_PLANE error.}
	\label{fig:NonPlanarity1}
\end{figure}

In figure \ref{fig:NonPlanarity1}, the blue plane depicts the adjusted plane which is, in this case, a least-squares plane. The plane in pink is the polygon which is defined by a linear ring (P1, P2, P3, P4, P1). The normal vector is perpendicular to the adjusted plane. The perpendicular distance between the plane and all points (P1, P2, P3, P4) are individually calculated and compared with the tolerance. 

According to the validation rules by \citep{2014_SIG3D}, all vertices of a polygon must lie on the plane within a specified tolerance (e.g. 1 cm). For example, if the perpendicular distance between P1 and the fitted plane exceeds a predefined tolerance of 1 cm, then the polygon is invalid according to the standard. In reality, the deviation is sometimes so small (e.g. 1 mm), and thus a large number of polygons are invalid due to DISTANCE\_PLANE error. So far this is the most common error in 3D building models \citep{2016_3dgeoinfo_citygml_errors}. The figure \ref{fig:np_polygon_gs} shows a non-planar polygon (\enquote{Polygon 4}) on the ground surface in which all vertices do not lie in the adjusted plane. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{./files/healing_3.pdf}
	\caption{Non-planar polygon on the ground surface.}
	\label{fig:np_polygon_gs}
\end{figure}

\subsubsection{NORMALS\_DEVIATION Error}
According to \citep{2016_3dgeoinfo_citygml_errors}, the NORMALS\_DEVIATION error can be caused by small folds in the polygon which is due to a very small distance between vertices within the polygon and might not be detected with the DISTANCE\_PLANE tolerance. 

In figure \ref{fig:NonPlanarity2}, the top polygon of a solid is defined by an exterior linear ring in which the point sequences are (P1, P2, P3, P4, P5, P6, P7, P8, P1). The points (P1, P2, P3, P4) as well as (P5, P6, P7, P8) are not collinear. The distance between points (P2 to P3) and (P6 to P7) are very small (e.g. 1 mm) which causes folds in the polygon. If the DISTANCE\_PLANE tolerance value is 1 cm, in this case, would not be able to detect the error; therefore another tolerance value needs to be defined.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{./files/NonPlanarity_2.pdf}
	\caption[Small folds in the polygon]{NORMALS\_DEVIATION error (based on: \citep{2013_solid_validation}).}
	\label{fig:NonPlanarity2}
\end{figure}

\newpage
\subsubsection{Floating Point Number Error}
According to \citep{2016_3dgeoinfo_citygml_errors}, semantic 3D buildings have a very little topological relation between the geometries. For example, a cube-shaped geometry as a 3D building is represented by six independent polygons (an exterior linear ring). Each linear ring is created by the sequence of ordered points. So, each corner point is stored three times in the dataset. For example, (1.0, 1.0, 0.0), (1.0, 1.0, 0.001) and (1.0, 1.0, 0.0) represent the same point and have a slight difference in the z-coordinate, which can produce error including the non-planarity issue. The same situation can happen with a small difference in any of x and y coordinates.

Figure \ref{fig:flt_error1} shows the error situation occurred due to floating point number error where there is a tiny gap between three surfaces. The intersection point of three surfaces (Polygon 1, Polygon 2 and Polygon 3) of a solid must the located in the same position, but in this case, the intersection point in \enquote{Polygon 1} is not located at the exact position. 

\begin{figure*}[h]
	\centering
	\begin{tabular}{c}
		\includegraphics[width=0.80\linewidth]{./files/fpe_gaps.pdf}
	\end{tabular}
	\caption[Cracks in the solid]{Cracks in the solid due to floating point number error.}
	\label{fig:flt_error1}
\end{figure*}

%\newpage
Figure \ref{fig:flt_error2} illustrates the same situation like figure \ref{fig:flt_error1} but intersects with the other surfaces. In this case, polygon belongs to the wall surface (Polygon 1) intersecting with a polygon at the roof surface. So, the computation of the floating point numbers during the creation or automatic generation of models must be handled very carefully. Otherwise, this can cause a slight difference in the 3D point coordinates which might create the planarity issue. Apart from planarity error, the floating point number error can cause other errors like intersecting with other surfaces, gaps, or other types of geometric error, so the model is invalid.

\begin{figure*}[h]
	\centering
	\begin{tabular}{c}
		\includegraphics[width=0.80\linewidth]{./files/fpe_overlaps.pdf} 
	\end{tabular}
	\caption[Intersection of surfaces]{Intersection of surfaces due to floating point number error.}
	\label{fig:flt_error2}
\end{figure*}

