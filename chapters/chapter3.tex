\chapter{Related Work \label{cha:chapter3}}
\citep{2013_Geom_validation_NAlam} stated the validity requirement of 3D geometric and semantic consistency based on the CityGML and GML specification. They defined validation rules for polygons as well as linear rings. \citep{2013_Geometric-semantical_consistency_validation} further developed the validation rules for semantic and geometric consistency of the CityGML models. They addressed the planarity error with CP\_PLAN error identifier which is sub-divided into CP\_PLANDIST, CP\_PLANDISTALL, and CP\_PLANTRI errors. The CP\_PLANDIST is the DISTANCE\_PLANE error and CP\_PLANDISTALL is basically the NORMALS\_DEVIATION error. CP\_PLANTRI can detect folds and sharp bends in the polygon.

\citep{2013_N_alam_automatic_healing} presented their research work on geometric validation and implemented an automatic validation and repair algorithm for the 3D buildings. The \verb+CityDoctor+\footnote{CityDoctor validation tool: http://www.citydoctor.eu/} tool can validate and analyze errors including planarity error in CityGML building models. For planarity error, \verb+CityDoctor+ validation tool uses first three vertices (to find CP\_PLANDIST error) of a linear ring to define the plane and the default planarity tolerance is 0.01 m. Initially, 1164 buildings were tested with the \verb+CityDoctor+ tool. The validation result shows that 789 buildings have geometric errors in which 1765 polygons have the planarity error. There are other errors present in the dataset such as self-intersections of planes, holes in the solid, wrong orientations of surface and etc.

\citep{2013_solid_validation} investigated ISO 19107 spatial schema and implemented the methodology for the validation of 3D primitives in a prototype called {\verb+val3dity+}\footnote{Geometric validation of 3D primitives: http://geovalidation.bk.tudelft.nl/val3dity/}. The non-planarity of polygons can be checked by their tool and they discussed the tolerances and methods to test the planarity. They recommended using least-squares plane-fitting to define the plane. {\verb+val3dity+} can be (figure \ref{fig:validity}) used to validate the real-world datasets which is accessible via an online platform or as a desktop application. The tool can be used to validate the following 3D geometric primitives:
\begin{itemize}
	\item Polygon
	\item MultiSurface and CompositeSurface
	\item Solid, MultiSolid and CompositeSolid
\end{itemize}

The \verb+val3dity+ platform uses three tolerances to validate the primitives; snap tolerance, planarity tolerance, and overlap tolerance. The default planarity tolerance is 0.01 m which can be customized. The validation report is returned to the user with their error codes. Error 203 and 204 represent the planarity error. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.80\textwidth]{./files/val3dity.JPG}
	\caption[val3dity online validation platform]{val3dity online validation platform \citep{2013_solid_validation}.}
	\label{fig:validity}
\end{figure}

Automatic validation and healing approach for the CityGML models is implemented by \citep{2013_N_alam_automatic_healing}. Their approach can heal the errors from 3D building models including the planarity problem. Their healing approach is different for different semantic surfaces (ground, wall and roof surfaces) which are depicted in figure \ref{fig:Error_GS_WS_RS}. The surface with smallest z-coordinate is identified as the ground surface and passing the z-coordinate of all points of the linear ring can heal the planarity error in the ground surface polygon. The same method is applied for the LoD1 and LoD2 flat roof surface but with the average z-value in z-coordinate. For all other roof types, a least square plane fitting is calculated for all roof surfaces and each point is projected along with the z-axis until the roof surfaces are planar or exceed the maximum number of iteration. The validation result shows that a building with 580 polygons has 79 planarity (CP\_PLAN) errors. The healing approach can heal 82\% of the errors including the planarity error. As the healing of planarity error depends on the identification of semantic surfaces and can produce the wrong result if the identification of surface is not correct. The volume of a solid can also be changed due to the repair. Another drawback of this healing process is the maximum number of iterations. The healing process stops if it reaches the maximum value of iteration which does not guarantee the repair of errors.

\begin{figure*}[h]
	\centering
	\begin{tabular}{cc}
		\includegraphics[width=0.47\linewidth]{./files/Error_GS_WS.JPG}& \includegraphics[width=0.55\linewidth]{./files/Error_RS.JPG}\\[0.1cm]
		(a)& (b)
	\end{tabular}
	\caption[Semantic surface-specific healing technique]{Healing of planarity errors: (a) ground and wall surfaces (b) roof surface \citep{2013_N_alam_automatic_healing}.}
	\label{fig:Error_GS_WS_RS}
\end{figure*}

Another way to heal the planarity error by doing the triangulation of the non-planar polygon \citep{2013_N_alam_automatic_healing}. A non-planar polygon might be split into multiple triangular polygons which can heal the planarity error. A polygon made of three vertices is always planar. The \verb+GeometryValidator+ \citep{2016_ogc_qie} within the Feature Manipulation Engine (FME \footnote{Feature Manipulation Engine: https://www.safe.com/}) workspace uses the triangulation technique to heal the non-planar polygons though the algorithm can end up with numerous triangular polygons if the polygon consists of a large number of points. For example, the triangulation process can produce a finite number of triangular polygons depending on the size of the input polygon. However, The healing accuracy for this kind of approach is 100\%.

\citep{2016_automatic_enhancing_lod2_buildings} described the method for the automatic enhancing from LOD2 to LOD2+ models with the specification of indoor geometries based on its exterior geometries. The CityGML LOD2 models were used as an input and the validity of those models was a prerequisite. They used constrained triangulation technique to the non-planar polygons to ensure the planarity. A non-planar polygon is split into multiple triangular polygons which makes them perfectly planar as depicted in figure \ref{fig:Triangulation}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.60\textwidth]{./files/Triangulation.JPG}
	\caption[Polygon triangulation technique]{Triangulation of polygons \citep{2016_automatic_enhancing_lod2_buildings}.}
	\label{fig:Triangulation}
\end{figure}

A sophisticated investigation on the common errors in CityGML datasets are conducted by \citep{2016_3dgeoinfo_citygml_errors} as a continuation of research work done by \citep{2013_solid_validation} which is a complete overview of commons errors present in all 3D geometric primitives. Non-planarity of the polygon is identified as the most common error in 3D building models. They validated 40M surfaces and 3.6M buildings from 37 CityGML datasets from different sources in which almost 40\% of the dataset is invalid due to planarity error. Some datasets can have full of specific errors. Errors can be produced by the modeling software or during the conversion and model optimization.

%\newpage
A guide for modeling 3D object with a set of validation rules for GML and CityGML models are proposed by SIG3D\footnote{Special Interest Group 3D: http://www.sig3d.org} \citep{2014_SIG3D} quality working group. The details about the validation rules and modeling guidelines were explained for the 3D geometric primitives especially for polygons and linear rings. They also described the planarity and the tolerances.

According to \citep{2016_ogc_qie}, a web-based validation framework implemented by Lukas which is an XML based validation technique. A user sends a CityGML instance document along with validation plan to the OGC Web Processing Service (WPS) and the WPS validates instance documents against the validation plan and an error report is sent back to the user. Figure \ref{fig:virtualcity_validator} shows the architecture overview of the \verb+virtualcity+ validation framework.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.70\textwidth]{./files/virtualcity_validator.JPG}
	\caption[virtualcity validation framework]{virtualcity validation framework \citep{2016_ogc_qie}.}
	\label{fig:virtualcity_validator}
\end{figure}

An overview of CityGML dataset quality has been presented by \citep{2016_ogc_qie}  where the planarity issue of the polygon is especially mentioned. The tolerances are briefly touched upon and the different validation techniques by researchers are compared.

An automatic repair approach for invalid polygons was done by \citep{2012_repairing_invalid_polygons}, in which they define a validation and repair method based on Simple Feature Specifications (SFS). The main focus of the research is to repair any invalid polygons with errors like self-intersections, collapsed area, disconnected interior, dangling pieces, overlapping boundaries and etc. But, they did not address the non-planarity of the polygons and did not fix them. \citep{2012_Zhao_hand_made_3D_model} also attempted to repair the hand-made 3D building models, but the method only fixes the incompleteness and separation errors on the 3D buildings.

\citep{2013_Zhao_shrink_wrapping} presented the automatic repair of CityGML LOD2 buildings using the shrink-wrapping method. The shrink-wrapping healing approach can repair errors like cracks, intersections or overlaps between surfaces. The algorithm is able to generate a valid watertight bounding shell which is basically the exterior of the building. The same authors \citep{2014_zhao_citygmlrepair} published their work on the automatic repair in which they shortly addressed the planarity issue and did not explain the repair method in detail. 

The repair of planarity error on the roof surfaces was recently investigated by \citep{2018_Planarization} using a linear optimization. The algorithm only repairs the planarity error in the roof surfaces by optimizing the z-coordinate values in the 3D point coordinates. The algorithm can preserve the characteristics of the roof surfaces and can avoid disturbing edges.

\citep{2007_Kramer} presented a quality measure of 3D city models and addressed common parameters used for the spatial quality measurement. Mathematical formalism for spatial data quality is stated which can be used to create algorithms for measurement and improvement the quality of 3D city model. According to the research, conceptual reality can be used to measure quality (e.g. completeness, positional and semantic accuracy, correctness, logical consistency, temporal conformance). But they did not address geometric consistency (e.g. non-planarity of polygons) during their research. 

Acquisition and integration of 3D geodata for the Berlin's virtual 3D city model were explained in \citep{2006_berlin_3D_city_model}. The geometry of 3D building model has been captured by the photogrammetry and laser-scanning based data acquisition techniques. The generated 3D building models are available at various levels of detail (LOD). The 3D editor system was also used to model specific 3D objects like architectural building models (LOD3). It is possible that the algorithm for generating or processing such 3D building models may introduce planarity error and possibly other errors. For example, if the DISTANCE\_PLANE tolerance was not considered while extracting the surfaces from the 3D point cloud acquired by the laser-scanning.

\citep{2009_Nagel} developed the consequential requirements and discussed on the automatic reconstruction of Building Information Models (BIM). A two-step reconstruction method was used, (1) from graphics models to CityGML building models, and (2) from CityGML models to Industry Foundation Classes (IFC) building models. Conversion from graphics model to CityGML, random errors can be introduced. Floating point number error can also be produced during the processing or optimization of the model.

\citep{2011_Groeger} presented a method to check the consistency of the 3D models to assure the usability and the suitability of the underlying model. For the consistency check, the components like solids and surfaces assume that the faces are always planar but sometimes they are not. Another detailed quality assessment (e.g. systematic, random and gross errors) of the 3D building is conducted by \citep{2010_Quality_buildings}.

