\chapter{Validation and Automatic Repair Methodology \label{cha:chapter4}}
There are a certain set of validation rules defined in ISO 19107 and in CityGML standard. The violation of validation rules during the creation of the 3D building model can cause errors. There are some existing validation and repair techniques available but there is no exact rule that one must follow to heal the errors. The researchers are free to choose any suitable techniques to repair the 3D building models according to the specific requirement. 

The recent developments and methodologies to repair planarity errors are described in chapter \ref{cha:chapter3} which are capable of repairing errors in the 3D building model, even though there is some ongoing research in this field. However, the focus of this work is to validate and repair the errors that are responsible for non-planarity of the polygons. 

The next section describes the validation rules, the repair methodology and explains the following: 
\begin{itemize}[leftmargin=*]
	\item How is the model invalid due to planarity error?
	\item Different aspects of planarity error?
	\item What are the tolerances and how to define them?
	\item What are the error dependencies?
	\item Is the preprocessing of input dataset necessary?
	\item How to detect small folds and repair them?    
	\item How to detect and repair rounding error?
	\item How to repair planarity errors?
	\item What are the impacts of the repair process?
	\item How accurate is the repaired building model?
	\item Is there any new planarity error or other error created during the repair process?
\end{itemize}

\section{Validity Criteria \label{sec:planarity}}
\subsection{Tolerances \label{sec:tol}}
The tolerances are the user-defined values depending on the requirements of the application or desired accuracy. The definition of tolerance depends on the purpose of use that the data is to be used as an input or to be processed. The tolerance for the DISTANCE\_PLANE error in this paper is addressed by $\epsilon_{1}$ and NORMALS\_DEVIATION is by $\epsilon_{2}$. The $\epsilon_{3}$ is used to indicate snapping tolerance to detect the floating point number error. The section \ref{sec:nPlanarity} explains all aspects of planarity error in the 3D building models. 

\subsubsection{Tolerance for DISTANCE\_PLANE Error} 
The tolerance $\epsilon_{1}$ is a user-defined value used in the validation process. The selection of the $\epsilon_{1}$ is important during the validation process because of the heterogeneous datasets. Due to the different types of data acquisition techniques and generation of 3D buildings, some datasets are full of specific errors \citep{2016_3dgeoinfo_citygml_errors} and an individual tolerance can be defined. The polygon contains one exterior linear ring and might have zero or more interior linear rings. The $\epsilon_{1}$ is same for both interior and exterior linear rings.

\subsubsection{Tolerance for NORMALS\_DEVIATION Error} 
If the deviations of the normal vector of a plane and all other normals are greater than a predefined tolerance $(\epsilon_{2})$, the {polygon} is not planar. This can be checked by two ways; firstly, by calculating the distance between two consecutive points (e.g. 1 mm), secondly, by the deviation of the normal vector (e.g. 80$^{\circ}$) of the plane to all other planes. For example, the DISTANCE\_PLANE tolerance of 1 mm can not detect NORMALS\_DEVIATION error (figure \ref{fig:NonPlanarity2}).

\subsubsection{Snapping Tolerance \label{sec:flt_error}}
Slight difference in the point coordinates due to inefficient handling of floating point numbers can be detected by using the snapping tolerance. The distance from each point to all points from adjacent polygons can be compared to find such error. Snapping tolerance is a user-defined value based on the dataset quality or can be an application specific requirement. For example, $\epsilon_{3}$ can be defined as 1 mm or 0.01 mm, before the validation and automatic repair process.

\subsection{Error Dependencies \label{sec:err_dep}}
The planarity error is occurred at the polygon level and is dependent on the linear ring. For example, if the linear ring contains less than three points, then the linear ring is invalid and can not be processed. Connectivity and completeness of the geometry are important while processing the geometry. The wrong connectivity between the geometries can cause the wrong result during the processing. For instance, the adjacency information of an incident vertex or face can have a big impact while detecting the floating point number error. The plane can not be defined with three collinear vertices. The repair result can be influenced if there are self-intersections of polygons or the polygon is not closed.

\subsection{Halfedge Data Structure \label{sec:hds}}
The early stage of the processing, a proper data structure for the building geometry is necessary to process and to navigate through the geometries; therefore, the geometry of 3D building must be stored with their incidence information of vertices, edges, and faces. This information can be used to find the adjacency of vertices, edges, and faces.

The edge-centered data structure can be used to store the connectivity information between the incident vertices, edges, and faces. According to \citep{2017_cgal_hds}, in Halfedge Data Structure (HDS), every edge can be decomposed into two half-edges. HDS is illustrated in figure \ref{fig:hds} where the orientation of an edge can be a halfedge and an opposite halfedge. The HDS is capable of maintaining the incidence relationship of vertices, edges, and faces due to their connectivity information. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.60\textwidth]{./files/hds2.PNG}
	\caption[Halfedge data structure]{Halfedge data structure \citep{2017_cgal_hds}.}
	\label{fig:hds}
\end{figure}

A valid topological relation between surfaces of the building geometry can be utilized while validating the dataset. A correct adjacency information of an incident vertex or edge or face is very meaningful. For instance, 2-manifoldness, adjacency with other faces, faces around vertex or vertices around a vertex, and etc. The adjacent faces can be found by using the face incidence information which is useful to find floating point number error.

\section{Planarity Error Validation \label{sec:validation}}
\subsection{Validation Rules \label{sec:valid_rules1}}
For different planarity error aspects, the validation rules are also different:

\textbf{Rules for DISTANCE\_PLANE error}: If the perpendicular distance between the least-squares plane and any of the vertex of the polygon is greater than a given tolerance $(\epsilon_{1})$, the polygon is not planar. 
\begin{equation} \label{eq:tol_1}
	distance\_plane > \epsilon_{1} 
\end{equation}
\begin{tabbing} 
	where \hspace{0.6cm} \= $distance\_plane$ = perpendicular distance from a point to the adjusted plane\\
	\> $\epsilon_{1}$ = user-defined tolerance (e.g. 1 cm)\\
\end{tabbing}

\textbf{Rules for NORMALS\_DEVIATION error}: If any two consecutive vertices in the same polygon are too close to one another and cause small folds in the polygon; therefore, the distance between them would be sufficient to find such error. So, $\epsilon_{2}$ needs to be defined to find the closeness between two consecutive vertices within the polygon. There could be another way to define this value by defining the deviation (e.g. 80$^{\circ}$) of the normal vector. If the distance between two consecutive vertices is smaller than the predefined tolerance ($\epsilon_{2}$), there polygon has NORMALS\_DEVIATION error.
\begin{equation} \label{eq:tol_2}
	consecutive\_dist < \epsilon_{2}
\end{equation}
\begin{tabbing} 
	where \hspace{0.6cm} \= $consecutive\_dist$ = distance between two consecutive vertices within\\
	\> the same polygon\\
	\> $\epsilon_{2}$ = user-defined tolerance (e.g. 1 mm)\\
\end{tabbing}

\textbf{Rule for floating point number error}: This is similar $\epsilon_{2}$ but the distance between two vertices from adjacent polygons is less than a predefined tolerance $(\epsilon_{3})$, then one of those points has floating point number error.
\begin{equation} \label{eq:tol_3}
	dist < \epsilon_{3} 
\end{equation}
\begin{tabbing} 
	where \hspace{0.6cm} \= $dist$ = distance between two vertices (vertices from adjacent polygons)\\
	\> $\epsilon_{3}$ = user-defined tolerance (e.g. 0.01 mm)\\
\end{tabbing}

\subsection{Distance between Two Vertices}
The algorithm uses the simple equation to detect both NORMALS\_DEVIATION and floating point number error. The distance between two 3D points $(P_1(x_1,y_1,z_1)$ and $P_2(x_2,y_2,z_2))$ can be calculated using the following equation:
\begin{equation} \label{eq:equ_dist}
	dist = \sqrt{(x_2-x_1)^2 + (y_2-y_1)^2 + (z_2 -z_1)^2}
\end{equation}

\subsection{Least-squares Plane-fitting \label{sec:leastSquare_plane}}
The least-squares plane fitting computes all possible combinations of all planes and finds the best fitting for the arbitrary number of points. The calculation of simple plane-fitting may not be sufficient if the exterior of the polygon is defined by more than three points. The scalar equation of the plane is shown in equation \ref{eq:equ_plane}.
\begin{equation} \label{eq:equ_plane}
	Ax+By+Cz+D=0
\end{equation}
\begin{tabbing} 
	where \hspace{0.6cm} \= $(x,y,z)$ = point coordinates\\
	\> $(A,B,C,D)$ = plane parameters\\
\end{tabbing}

The polygon may contain an arbitrary number of points and by choosing only first three non-collinear points may not be sufficient enough to define the plane. The chosen first three points might have the largest deviations to the plane and the distribution of other points are completely ignored. A more accurate and robust plane-fitting algorithm is necessary to correctly define the plane. The researchers \citep{2016_ogc_qie,2016_3dgeoinfo_citygml_errors} recommended to use a least-squares plane-fitting algorithm to the find the best plane fitting for the polygons. 

\subsection{Distance between a Point and the Adjusted Plane \label{sec:dist_pln}}
The perpendicular distance to the adjusted plane from a point can be calculated and compared to the predefined tolerance value. The distance between a point and the plane is calculated using the following equation \ref{eq:dist_plane}:
\begin{equation} \label{eq:dist_plane}
	distance\_plane = \dfrac{Ax+By+Cz+D}{\sqrt{A^2+B^2+C^2}}
\end{equation}
\begin{tabbing} 
	where \hspace{0.6cm} \= $(x,y,z)$ = point coordinates\\
	\> $(A,B,C,D)$ = plane parameters\\
	\> $distance\_plane$ = perpendicular distance from a point to the adjusted plane\\
\end{tabbing}

\section{Repair Methodology \label{sec:repair}}
There is ongoing research works for the repair of planarity errors in 3D building models. Triangulation of non-planar polygon is one of the most popular technique to heal the planarity error. Another repair approach is dependent on the surfaces and the healing is performed differently for different semantic surfaces (wall surface, roof surface, and ground surface). The research focus of the thesis is to find a unique repair approach all planarity aspects and a generalized way which could be applicable to any polygons at any semantic surfaces.

This thesis proposes a three-step repair approach which can be used to heal all aspects of planarity error. The hierarchical repair techniques can be applied to heal the planarity error:

\begin{itemize}[leftmargin=*]
	\item Repair folds in the polygon 
	\item Repair floating point number error
	\item Repair planarity error  
\end{itemize}

\subsection{Repair Folds in the Polygon \label{sec:rep_folds}}
The calculation of the distance between two consecutive vertices can detect the closeness between them. The repair of small folds in the polygon can be healed by removing any of them. After repair (figure \ref{fig:healing_fpe_1}), the polygon has no folds. Now, it has the DISTANCE\_PLANE error which can be healed by the polygon splitting algorithm.

Figure \ref{fig:healing_fpe_1} illustrates the state of the model after repairing of NORMALS\_DEVIATION error depicted in figure \ref{fig:NonPlanarity2} in which the vertices P2 and P7 are removed by the repair approach. This approach can find the consecutive same vertices and also repair them.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{./files/healing_1.pdf}
	\caption{Repair of small folds in the polygon.}
	\label{fig:healing_fpe_1}
\end{figure}

The step by step repair technique to heal small folds in the polygon is as follows:

\begin{table}[H]
	\vspace{3pt}
	\centering
	\caption{Pseudo algorithm for the healing of folds in the polygon}
	\begin{tabular}{l}
		\hline
		\textbf{Algorithm 1}. Repair technique for small folds in the polygon\\
		\hline
		1: compute the distance between two consecutive vertices within the polygon \\
		 \ \ \ \textbf{consecutive\_dist}, except first and the last (e.g. P1 and P8)\\
		2: detection of small folds \\
		\ \ \ \ \ \ \textbf{if} ($consecutive\_dist < \epsilon_2$)\\ 
		\ \ \ \ \ \ \ (e.g. $\epsilon_2 = 1 mm$)\\         
		\ \ \ \ \ \ \ \ \ \ \ remove one vertex\\  
		\ \ \ \ \ \ \ \ \ \ \ repeat step 1 and check next two consecutive vertices\\          		                           
		\ \ \ \ \ \ \textbf{end if}\\
		\ \ \ \ \ \ \textbf{else}\\ 
		\ \ \ \ \ \ \ \ \ \ \ no folds in the polygon\\           
		\ \ \ \ \ \ \textbf{end else}\\
		3: continue to the next step\\
		\hline
	\end{tabular}  
	\label{tab:alg0}
\end{table}

\subsection{Repair Floating Point Number Error \label{sec:rep_flt}}
Unlike folds in the polygon, this error is dependent on the adjacent faces. In 3D buildings, vertices from all faces are stored independently which means, a single vertex is stored on all its incident faces. The simple distance calculation between a vertex from the incident face and all vertices from adjacent faces can find the floating point number errors easily.

Figure \ref{fig:dup_before_rep} illustrates the floating point number error situation where P3 is stored three times (P3a, P3b, P3c) which represent the same 3D point. Point P3a and P3b belong to \enquote{Face 1} (wall surface) and \enquote{Face 2} (wall surface) respectively. Point P3c is incident to \enquote{Face 3} (roof surface). Note that, point P3b$(x_b,y_b,z_b)$ and P3c$(x_c,y_c,z_c)$ are exactly in the same location and the distance between them is 0 mm. Due to small deviation in the coordinates of point P3a$(x_a,y_a,z_a)$, the P3a is not located in the same position like P3b and P3c. The deviation is 1 mm. So, P3a is incident to only \enquote{Face 1} where P3(b,c) is incident to \enquote{Face 2} and \enquote{Face 3}. The adjacency for both vertices are:

\begin{itemize}[leftmargin=*]
	\item Adjacency of P3a = 1
	\item Adjacency of P3b = 2 (where P3b=P3c)
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.80\textwidth]{./files/dup_before_rep.pdf}
	\caption{Floating point number error.}
	\label{fig:dup_before_rep}
\end{figure}

Table \ref{tab:alg1} shows the step by step repair method in the form of pseudo algorithm to repair the rounding error.
\begin{table}[H]
	\vspace{3pt}
	\centering
	\caption{Pseudo algorithm for the healing of floating point number error}
	\begin{tabular}{l}
		\hline
		\textbf{Algorithm 2}. Repair technique for floating point number error\\
		\hline
		1: compute the distance between a vertex to another (vertices from \\
		\ \ \ \ adjacent faces), \textbf{dist}\\
		2: validation of floating point number error \\
		\ \ \ \ \ \ \textbf{if} ($dist < \epsilon_3$)\\ 
		\ \ \ \ \ \ \ (e.g. $\epsilon_3 = 1 mm$)\\         
		\ \ \ \ \ \ \ \ \ \ \ one vertex has error\\
		\ \ \ \ \ \ \ \ \ \ \ find the adjacency of both vertices (P3a, P3b)\\
		\ \ \ \ \ \ \ \ \ \ \ P3a is occurred adj\_P3a and P3b is occurred adj\_P3b times\\                
		\ \ \ \ \ \ \ \ \ \ \ \ \ \textbf{if} ($adj\_P3a < adj\_P3b$) \\    
		\ \ \ \ \ \ \ \ \ \ \ \ \ \ P3a is replaced by P3b\\        
		\ \ \ \ \ \ \ \ \ \ \ \ \ \textbf{end if}\\    
		\ \ \ \ \ \ \ \ \ \ \ \ \ \textbf{else if} ($adj\_P3a > adj\_P3b$) \\    
		\ \ \ \ \ \ \ \ \ \ \ \ \ \ P3b is replaced by P3a\\        
		\ \ \ \ \ \ \ \ \ \ \ \ \ \textbf{end else if}\\
		\ \ \ \ \ \ \ \ \ \ \ \ \ \textbf{else}\\    
		\ \ \ \ \ \ \ \ \ \ \ \ \ \ P3a is replaced by P3b  or vice versa\\        
		\ \ \ \ \ \ \ \ \ \ \ \ \ \textbf{end else}\\                                
		\ \ \ \ \ \ \textbf{end if}\\
		\ \ \ \ \ \ \textbf{else}\\ 
		\ \ \ \ \ \ \ \ \ \ \ no floating point number error\\
		\ \ \ \ \ \ \ \ \ \ \ goto step 1 and chose next vertex until all vertices from other faces\\                
		\ \ \ \ \ \ \textbf{end else}\\
		3: continue to the next step\\
		\hline
	\end{tabular}  
	\label{tab:alg1}
\end{table}

Finally, error in P3a is identified. This duplicate point can be removed by replacing the vertex P3a by the P3b (or P3c) as depicted in figure \ref{fig:dup_after_repair}. The repair approach is dependent on the adjacency information of the vertices, and they might have the same adjacency. In this case, the algorithm chose any of them to replace the other one. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.76\textwidth]{./files/dup_after_rep.pdf}
	\caption{Repair of floating point number error.}
	\label{fig:dup_after_repair}
\end{figure}

Planarity errors can be healed by the repairing of floating point number error which is illustrated in figure \ref{fig:healing_fpe_2} (b). \enquote{Polygon 4} belongs to the ground surface and has the largest deviation to the adjusted plane (figure \ref{fig:healing_fpe_2} (a)) due to slight difference in the z-coordinate. The point of intersection by \enquote{Polygon 1}, \enquote{Polygon 2} are on the adjusted plane but \enquote{Polygon 4} not. For this reason, the solid has a gap (between wall and ground surface), and also planarity error. So, the repair of floating point error, in this case, heals both gap and planarity error.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{./files/healing_2.pdf}
	\caption[Healing of planarity error by repairing floating point number error]{Planarity error can be healed by repairing the floating point number errors.}
	\label{fig:healing_fpe_2}
\end{figure}

The repair of floating point number error heals rounding error and thus the geometric topology is consistent and no duplicate point (due to rounding error) is present. But there might be the case that planarity errors still exist even after healing of floating point error. It is also possible that there might be other errors in the dataset which are not covered or taken into account during repair.

\newpage
\subsection{Repair Planarity Error \label{sec:ps_algo}}
The repair of floating point number error ensures that there are no duplicate points but does not guarantee to remove the planarity error. Even a new planarity error can be introduced and must be repaired in this step. Triangulation of non-planar polygon produces a finite number of triangular facets to heal the planarity error. This polygon splitting methodology can produce the lowest number of facets depending on the positional accuracy and distribution of the points in the polygon. 

The algorithm splits a non-planar polygon. The idea is to create multiple polygons from a non-planar polygon where the algorithm defines a least-squares plane with first three non-collinear vertices and checks if the next vertex is in the new plane (within a predefined repair threshold). The algorithm compares the tolerance and the threshold before adding the vertex to the polygon. Otherwise, the algorithm splits the plane defining vertices to a new polygon and defines again another new plane with three non-collinear points. This process continues to the last vertex of the non-planar polygon.

The perpendicular distance between the adjusted plane and each vertex must be less than a threshold value which is the repair tolerance value (e.g. 0.0001 mm). The repair tolerance must be smaller than the DISTANCE\_PLANE tolerance value otherwise the repair algorithm may have no effect on the non-planar polygon. The selection of the repair tolerance depends on the expected output accuracy of the repaired model or the requirement of the intended application of use.

An example of a non-planar polygon is depicted in figure \ref{fig:face_split_points} (a) with the repair steps.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{./files/face_split_points.pdf}
	\caption{Polygon splitting algorithm.}
	\label{fig:face_split_points}
\end{figure}

%\newpage
The healing approach consists of several steps and the step by step procedures for the polygon splitting algorithm in figure \ref{fig:face_split_points} are:
\\
\textbullet\ \textbf{Step 1:} The first three non-collinear points (P1, P2, and P3) define the plane.  (figure \ref{fig:face_split_points} (b))\\
\textbullet\ \textbf{Step 2:} Calculate perpendicular distance from point P4 to the plane according to equation \ref{eq:equ_plane} and compare with the repair tolerance (e.g. 0.0001 mm). This case, the distance is smaller than the repair tolerance and point P4 belongs to the same plane. (figure \ref{fig:face_split_points} (c)) \\
\textbullet\ \textbf{Step 3:} Repeat step 2 (but with next point P5) and compare with the repair tolerance. This case, the distance is larger than the repair tolerance, and the point belongs to the plane. So, the polygon must be split now and the newly created polygon, in this case, would be (P1, P2, P3, P4, P1). \\
\textbullet\ \textbf{Step 4:} Define a new plane by selecting the first and the last vertices from the previously split polygon, and the next point. So, this case the new plane defining points would be P1, P4, and P5. Repeat step 2 and step 3 till to the last vertex of the polygon. As P5 is the last point \ref{fig:face_split_points} (d) and now must be split to a new polygon.

Finally, the non-planar polygon was split into two multiple polygons. \enquote{Newly split polygon 1} with the point sequences (P1, P2, P3, P4, P1) and \enquote{Newly split polygon 2} with (P1, P4, P5, P1). Compared to the triangulation solution for the non-planar polygons, the suggested algorithm can produce few multiple polygons. The same orientation for a newly created polygon is maintained as in the input dataset. Figure \ref{fig:face_split} shows the different states of a non-planar polygon in a 3D building before and after repair. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{./files/face_split.pdf}
	\caption{3D building model before and after healing.}
	\label{fig:face_split}
\end{figure}

\newpage
The pseudo-algorithm for the polygon splitting technique is as follows: 

\begin{table}[H]
	\vspace{3pt}
	\centering
	\caption{Pseudo algorithm for the healing of planarity error}
	\begin{tabular}{l}
		\hline
		\textbf{Algorithm 3}. Healing of non-planar polygon\\
		\hline
		1: define a least-squares plane with the first three non-collinear points of polygon\\
		2: define a repair tolerance (\textbf{rep\_tol}) \\
		3: compute the perpendicular distance between the plane\\
		\ \ \ \ and the next vertex (\textbf{distance\_plane})\\
		4: polygon splitting starts: \\
		\ \ \ \ \ \ \textbf{if} (\textbf{distance\_plane} $<$ \textbf{rep\_tol})\\ 
		\ \ \ \ \ \ (e.g.\textbf{rep\_tol} = 0.0001 mm)\\        
		\ \ \ \ \ \ \ \ \ \ \ the vertex belongs to the plane and repeat step 3 \\
		\ \ \ \ \ \ \textbf{end if}\\
		\ \ \ \ \ \ \textbf{else}\\ 
		\ \ \ \ \ \ \ \ \ \ \ split the polygon with all points \\
		\ \ \ \ \ \ \ \ \ \ \ that are previously considered in the plane (newFace)\\
		\ \ \ \ \ \ \ \ \ \ \ define a new plane with three non-collinear points\\
		\ \ \ \ \ \ \ \ \ \ \ plane defining points:\\        
		\ \ \ \ \ \ \ \ \ \ \ \ \ $1^{st}$ point = $1^{st}$ vertex of newFace\\
		\ \ \ \ \ \ \ \ \ \ \ \ \ $2^{nd}$ point = last vertex of newFace\\
		\ \ \ \ \ \ \ \ \ \ \ \ \ $3^{rd}$ point = next point of newFace\\
		\ \ \ \ \ \ \ \ \ \ \ goto step 3 and repeat till last vertex of non-planar polygon\\                                
		\ \ \ \ \ \ \textbf{end else}\\
		5: write final output\\
		\hline
	\end{tabular}  
	\label{tab:alg2}
\end{table}
