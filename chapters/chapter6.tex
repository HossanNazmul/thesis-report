\chapter{Experimental Results and Discussion \label{cha:chapter6}}
This chapter addresses the outcome of the implemented prototype and the limitations of the current approach. There might be other errors in the dataset which can influence the validation and repair results. The next section demonstrates the experimental results, challenges and problems in details. 

\section{Experimental Results \label{sec:ex_res}}
The outcome of the repair algorithm gives a promising result and can heal the planarity error as expected. The LOD2 CityGML building models shown in figure \ref{fig:Testdatasets} which are downloaded via Berlin 3D – Download Portal\footnote{3D – Download Portal: http://www.businesslocationcenter.de/berlin3d-downloadportal/}.

\textbf{Test datasets and configurations}\\
The datasets listed in table \ref{tab:test_data} are checked for all aspects of planarity error. The automatic validation and repair algorithm is dependent on the predefined tolerances which should be defined by the user. These tolerances can be changed depending on the input datasets and desired output planarity accuracy. The default tolerances are listed in table \ref{tab:test_data_config}: \\

\newpage
\begin{figure*}[h]
	\centering
	\begin{tabular}{cc}
		\includegraphics[width=0.35\linewidth]{./files/Test-data-1.JPG}& \includegraphics[width=0.30\linewidth]{./files/Gendarmenmarkt.JPG} \\[0.1cm]
		(1) Test data 1 & (2) Gendarmenmarkt\\[0.1cm]
		\includegraphics[width=0.35\linewidth]{./files/Hedwigskathedrale.JPG}&
		\includegraphics[width=0.40\linewidth]{./files/Bodemuseum-simple.JPG}  \\[0.1cm]
		(3) Hedwigskathedrale& (4) Bodemuseum \\[0.1cm]
		\includegraphics[width=0.40\linewidth]{./files/Tu-berlin.JPG}&
		\includegraphics[width=0.40\linewidth]{./files/Fernsehtrum.JPG}  \\[0.1cm]
		(5) Hauptgebaeude TU Berlin& (6) Fernsehturm \\[0.1cm]
	\end{tabular}
	\caption[LOD2 CityGML building models]{LOD2 CityGML building models (downloaded via Berlin's 3D city portal).}
	\label{fig:Testdatasets}
\end{figure*}

\begin{table}[h!]
    \begin{center}
        \caption{List of test datasets}
        \begin{tabular}{|c|c|}
            \hline
            \textbf{Dataset}& \textbf{Name} \\
            \hline
            1& Test-data-1.gml\\
            \hline
            2& Gendarmenmarkt.gml\\
            \hline
            3& Hedwigskathedrale.gml\\
            \hline 
            4& Bodemuseum.gml\\
            \hline
            5& Hauptgebaeude-TU-Berlin.gml\\
            \hline
            6& Fernsehturm.gml\\
            \hline             
        \end{tabular} 
        \label{tab:test_data}    
    \end{center}
\end{table}


\begin{table}[h!]
	\begin{center}
		\caption{Default tolerances for the test datasets}
		\begin{tabular}{|c|c|c|}
			\hline
			\textbf{Name}& \textbf{Tolerance}& \textbf{Value[mm]} \\
			\hline
			DISTANCE\_PLANE tolerance& $\epsilon_1$& 1.0\\
			\hline
			NORMALS\_DEVIATION tolerance& $\epsilon_2$& 0.01\\
			\hline
			Snap tolerance& $\epsilon_3$& 0.001\\
			\hline 
			Repair tolerance& $rep\_tol$& 0.0001\\
			\hline 
		\end{tabular} 
		\label{tab:test_data_config}    
	\end{center}
\end{table}

\newpage
A small distance is allowed between the vertices and the adjusted plane \citep{2014_SIG3D}. So, the planarity error can be application specific depending on the tolerance. As the tolerances are very small and a very precise geometric algorithm must be used to process the data. Imprecise arithmetic or geometric calculation of the plane parameters can produce wrong validation results. 

Table \ref{tab:comp_epsilon} compares the validation results due to imprecise calculations of the least-squares plane parameters. The implemented prototype and \verb+val3dity+ tool use the same functionality of CGAL's linear least-squares plane fitting which is dependent to \enquote{EPSILON}\footnote{Bug with least-squares plane: https://github.com/CGAL/cgal/issues/2676} value which can affect the computation. The default \enquote{EPSILON} value is 0.00001 which can sometimes produce imprecise calculation. More precise results can be achieved by decreasing the \enquote{EPSILON} value. The implementation uses this \enquote{EPSILON} value as 1e-15. The use of Eigen library is recommended by CGAL to get more efficient and faster solution. In table \ref{tab:comp_epsilon}, dataset 2, 4 and 5 have planarity error due to imprecise calculation of least-squares plane-fitting while using the same planarity criteria but precise calculation finds no non-planar polygons.

\begin{table}[h!]
	\begin{center}
		\caption[The effect of imprecise least-squares plane-fitting]{The effect of imprecise least-squares plane-fitting}
		\begin{tabular}{|c|c|c|c|c|}
			\hline
			\textbf{Dataset}& \textbf{No. of}& \textbf{$\epsilon_1$} & \textbf{Non-planar polygons}& \textbf{Non-planar polygons}\\
			& \textbf{polygons}& [mm]& (EPSILON=1e-5)& (EPSILON=1e-15)\\
			\hline
			1& 7& 1.0& 0& no error\\
			\hline
			2& 61& 1.0& 2& no error\\
			\hline
			3& 1699& 1.0& 0& no error\\
			\hline
			4& 942& 1.0& 2& no error\\
			\hline
			5& 203& 1.0& 75& no error\\
			\hline
			6& 1391& 1.0& 0& no error\\
			\hline 
		\end{tabular} 
		\label{tab:comp_epsilon}    
	\end{center}
\end{table}


As the selected datasets do not contain any planarity error for the default configuration parameters (table \ref{tab:test_data_config}); therefore, some imaginary error deviations are added to all datasets and these datasets are used to the experiments in the next sections. Table \ref{tab:test_data_img} shows the new datasets with the added imaginary deviations and number of non-planar polygons in the datasets.

\begin{table}[h!]
	\begin{center}
		\caption{Imaginary deviations added to the test datasets}
		\begin{tabular}{|c|c|c|c|}
			\hline
			\textbf{Dataset}& \textbf{Name}& \textbf{Added}& \textbf{No. of non-planar}\\
			& & \textbf{deviation}& \textbf{polygons}\\
			\hline
			1& Test data 1.gml& 1 cm& 1\\
			\hline
			2& Gendarmenmarkt.gml& 1 cm, 0.1 cm& 2\\
			\hline
			3& Hedwigskathedrale.gml& 1 cm, 0.1 cm& 2\\
			\hline 
			4& Bodemuseum.gml& 1 cm, 1 mm& 2\\
			\hline
			5& Hauptgebaeude TU Berlin.gml& 1 mm& 1\\
			\hline
			6& Fernsehturm.gml& 1 mm& 0\\
			\hline             
		\end{tabular} 
		\label{tab:test_data_img}    
	\end{center}
\end{table}

The test results in table \ref{tab:comp_plane} shows that the simple plane fitting can find the same planarity errors like the least-squares plane-fitting while both uses the same planarity criteria. It's may be The simple plane-fitting algorithm uses only first three non-collinear points to define the plane. On the other hand, the least-squares plane-fitting considers all possible combinations of all planes and finds the best-fitted plane for that polygon. So, the calculation of the plane parameters are different for both approach. The \citep{2016_ogc_qie} also suggests to use least-squares plane-fitting to define a plane for the validation of planarity error of the polygon. Unlike simple plane-fitting algorithm, the least-squares plane considers all points and all combinations to define the plane; therefore the prototype uses the least-squares plane fitting. In table \ref{tab:comp_plane}, the deviation of 1 mm can not be detected with the default DISTANCE\_PLANE tolerance value; therefore, dataset 4 has only one non-planar polygon while datasets 5 and 6 have no errors. The DISTANCE\_PLANE tolerance value should be decreased in order to find more errors in datasets 4, 5 and 6.

\begin{table}[h!]
    \begin{center}
        \caption[Comparison between simple and least-squares plane fitting]{Comparison between simple and least-squares plane fitting (tolerance 1 mm).}
        \begin{tabular}{|c|c|c|c|}
            \hline
            \textbf{Datasets}& \textbf{No. of}& \textbf{Simple}& \textbf{Least-squares}\\
            & \textbf{polygons}& \textbf{plane-fitting}& \textbf{plane-fitting}\\            
            \hline
            1& 7& 1& 1\\
            \hline
            2& 61& 2& 2\\
            \hline
            3& 1699& 2& 2\\
            \hline 
            4& 942& 1& 1\\
            \hline
            5& 203& 0& 0\\
            \hline
            6& 1391& 0& 0\\
            \hline 
        \end{tabular} 
        \label{tab:comp_plane}    
    \end{center}
\end{table}

The table \ref{tab:comp_poly} shows how the tolerance value can affect the validation results for the planarity error. The tolerance here is the distance from a point to the least-squares plane. From the validation result, it is clear that the smaller the tolerance, the more the planarity error can be found.

\begin{table}[h!]
    \begin{center}
        \caption{Non-planar polygons for different tolerance values}
        \begin{tabular}{|c|c|c|c|c|}
            \hline
            \textbf{Dataset} & \textbf{No. of polygons}& \textbf{Non-planar}& \textbf{Non-planar}& \textbf{Non-planar}\\
            & & ($\epsilon_1$= 1 mm)& ($\epsilon_1$= 1 mm)& ($\epsilon_1$= 0.01 mm)\\
            \hline
            1& 7& 0& 1& 1\\
            \hline
            2& 61& 1& 2& 2\\
            \hline
            3& 1699& 1& 2& 2\\
            \hline 
            4& 942& 1& 1& 2\\
            \hline
            5& 203& 0& 0& 1\\
            \hline
            6& 1391& 0& 0& 0\\
            \hline             
        \end{tabular} 
        \label{tab:comp_poly}    
    \end{center}
\end{table}

\newpage
The table \ref{tab:plan_results} shows the number of polygons before and after the repair. The number of polygons after the repair is increased due to the polygon splitting technique but the planarized model is planarity error-free (at least up to repair tolerance). Dataset 6 has no planarity error even though a deviation of 1 mm added to a polygon, because the polygons are made by only three vertices. This reason, dataset 6 will be always planar. Other datasets are planar up to a certain tolerance, in this case the tolerance is 0.0001 mm. That means if the validation algorithm defines a DISTANCE\_PLANE tolerance of 0.0001 mm, there will be no planarity error in the dataset. This functionality enables a new possibility for the applications to produce a tolerance-specific planarity error-free models.

\begin{table}[h!]
    \begin{center}
        \caption[Number of polygons before and after the repair]{Number of polygons before and after the repair (DISTANCE\_PLANE tolerance: 0.01 mm and repair tolerance: 0.0001 mm).}
        \begin{tabular}{|c|c|c|c|}
            \hline
            \textbf{Dataset}& \textbf{Before healing}& \textbf{Non-planar}& \textbf{After healing}\\
            & [No. of polygons]& \textbf{polygons}& [No. of polygons]\\    
            \hline
            1& 7& 1& 8\\
            \hline
            2& 61& 2& 69\\
            \hline
            3& 1699& 2& 1701\\
            \hline 
            4& 942& 2& 947\\
            \hline
            5& 203& 1& 204\\
            \hline
            6& 1391& 0& -\\
            \hline          
        \end{tabular} 
        \label{tab:plan_results}    
    \end{center}
\end{table}

The table \ref{tab:split_vertices} shows a promising repair results of the polygon splitting algorithm. The polygon splitting technique is capable of keeping the number of newly created polygons as low as possible. The repair result for dataset 2 produces several multiple polygon except polygon 2. The number of vertices of a newly created polygon depends on the positional accuracy of the vertices, position in the point sequence, the distribution of points in 3D space, and etc. Dataset 3 has only 2 non-planar polygons which have total 190 vertices. Each non-planar polygon is split into 2 new polygons; first non-planar polygon was split into polygon 1 with only 3 vertices and polygon 2 with the 37 vertices, and the second one was split into polygon 3 with 3 vertices and polygon 4 with 151 vertices. This result indicates that the might be the one of first three vertices in the sequence had the largest deviation to the adjusted plane. The results for dataset 5 is similar to triangulation because the non-planar polygon has only 4 vertices.

\begin{table}[h!]
	\begin{center}
		\caption{Number of newly created polygons after repair}
	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		\multirow{2}{*}{} \textbf{Dataset}& \multirow{2}{*}{} \textbf{Non-planar} & \textbf{No. of}  & \textbf{No. of} & \textbf{New polygons}\\
										  	&                    \textbf{polygons}&   \textbf{vertices}& \textbf{split}& \textbf{[vertices]} \\
		\hline 
		1&                     1& 5& 2& polygon 1 has 4 vertices \\
		&                    & & & polygon 2 has 3 vertices \\	
		\hline 	
		\multirow{2}{*}{} 2& \multirow{2}{*}{} 2&  44& 8& polygon 1 has 3 vertices \\
		&                    & & & polygon 2 has 35 vertices\\
		&                    & & & polygon 3 has 3 vertices \\
		&                    & & & polygon 4 has 3 vertices \\
		&                    & & & polygon 5 has 3 vertices \\
		&                    & & & polygon 6 has 3 vertices \\
		&                    & & & polygon 7 has 3 vertices \\
		&                    & & & polygon 8 has 3 vertices \\
		\hline 
		\multirow{2}{*}{} 3& \multirow{2}{*}{} 2&  190& 4& polygon 1 has 3 vertices \\
		&                    & & & polygon 2 has 37 vertices \\
		&                    & & & polygon 3 has 3 vertices \\
		&                    & & & polygon 4 has 151 vertices \\
		\hline 
		\multirow{2}{*}{} 4& \multirow{2}{*}{} 2&  134& 5& polygon 1 has 3 vertices \\
		&                    & & & polygon 2 has 37 vertices \\
		&                    & & & polygon 3 has 3 vertices \\
		&                    & & & polygon 4 has 3 vertices \\
		&                    & & & polygon 5 has 94 vertices \\		
		\hline 			
		\multirow{2}{*}{} 5& \multirow{2}{*}{} 1&  4& 2& polygon 1 has 3 vertices \\
		&                    & & & polygon 2 has 3 vertices \\
		\hline 
		\multirow{2}{*}{} 6& \multirow{2}{*}{} 0&  -& -& - \\
		\hline 							
	\end{tabular}
	\label{tab:split_vertices}    
	\end{center}	
\end{table}

After healing of planarity error, the algorithm checks if the newly created polygons are planar or not. The algorithm generates a detail test report regarding the validation and repair status. Section \ref{tab:plan_results_comp} represents the number of non-planar polygons before and after the healing. Here is the validation result for dataset 4 in which 2 non-planar polygons are split into 5 multiple polygons. The following test report shows the number of vertices and polygons after the repair.

\newpage
%\verbatiminput{./files/xml/Bodemuseum_Test_Report.txt}
%\lstinputlisting[language={}]{./files/xml/Bodemuseum_Test_Report.txt}
\lstset{
	language=xml,
	tabsize=3,
	%frame=lines,
	caption=Test report for the healing of planarity error (dataset 4),
	label=code:sample1,
	frame=shadowbox,
	rulesepcolor=\color{gray},
	xleftmargin=20pt,
	framexleftmargin=15pt,
	keywordstyle=\color{black}\bf,
	commentstyle=\color{OliveGreen},
	stringstyle=\color{blue},
	numbers=left,
	numberstyle=\tiny,
	numbersep=5pt,
	breaklines=true,
	showstringspaces=false,
	basicstyle=\footnotesize,
	emph={gml:Polygon,gml:LinearRing,gml:name},emphstyle={\color{magenta}}}
\lstinputlisting{./files/xml/Bodemuseum_Test_Report.txt}



Finally, the repaired 3D building models were tested with the \verb+val3dity+ (version: 2.1.0) before and after the repair process. The validation result from \verb+val3dity+ \citep{2013_solid_validation} and the implemented prototype were compared (table \ref{tab:plan_results_comp}). The non-planar polygons from the imaginary datasets can be correctly detected by the implemented prototype and the results are correct. Unfortunately, in table \ref{tab:plan_results_comp}, all validation results are not same in which \verb+val3dity+ can find less non-planar polygons. For example, the deviations are added to the z-coordinates of the points at the ground surface polygon (dataset 2 and dataset 3). The implemented prototype correctly identifies the non-planar polygons as expected but \verb+val3dity+ identifies less errors. The distance between the plane and the vertex is very dependent to the precise calculations of the least-squares plane or the aforementioned \enquote{EPSILON} value. One possible reason could be the selection of \enquote{EPSILON} value or the due to other error in the datasets.

The planarity error in the 3D building models are repaired successfully and there is no planarity error or other errors detected by the \verb+val3dity+ tool. The main motivation of using \verb+val3dity+ was to check the following aspects for the newly created models:

\begin{itemize}[leftmargin=*]
	\item Is there any planarity error in the repaired model?
	\item Does the healing process produce any new error? (e.g. geometric, topologic or semantic errors)
\end{itemize}

\begin{table}[h!]
    \begin{center}
        \caption[Comparison of the validation and repair results with val3dity]{Comparison of the validation and repair results with val3dity.}
        \begin{tabular}{|c|c|c|c|c|}
            \hline
            \textbf{Dataset}& \textbf{Methods}& \textbf{Tolerance}& \textbf{Non-planar} &\textbf{Non-planar}\\
            & &  ($\epsilon_1$) & \textbf{polygons} &\textbf{polygons}\\            
             & & & (before healing)& (after healing)\\
            \hline
            1& val3dity& 0.01 mm& 1 & no error\\
            & Implemented prototype& 0.01 mm& 1 & no error \\
            \hline
            2& val3dity& 0.01 mm& 1 & no error\\
            & Implemented prototype& 0.01 mm& 2 & no error\\
            \hline
            3& val3dity& 0.01 mm& 0 & no error\\
            & Implemented prototype& 0.01 mm& 2 & no error\\
            \hline
            4& val3dity& 0.01 mm& 1 & no error\\
            & Implemented prototype& 0.01 mm& 2 & no error\\
            \hline
            5& val3dity& 0.01 mm& 1 & no error\\
            & Implemented prototype& 0.01 mm& 1 & no error\\
            \hline
            6& val3dity& 0.01 mm& no error & no error\\
            & Implemented prototype& 0.01 mm& no error & no error\\
            \hline                                    
        \end{tabular} 
        \label{tab:plan_results_comp}
    \end{center}
\end{table}

Concerning the speed of the implemented prototype, the validation and repair process was pretty fast with a notebook (Intel core i3 processor and 4 GB ram) but of course, depending on the size of the input model. The large the dataset, the more the processing time. A very simple CityGML parsing tool has been implemented and used which might have influence to the computational performance.

\section{Problems and Discussion \label{sec:dissc}}
The implemented algorithm focuses especially in the planarity error as well as other aspects that may cause planarity error. Floating point number error and folds in the polygon are also taken into account during validation. Besides planarity error, the algorithm also repair floating point number error and small folds in the polygon which can improve the geometric quality of the model. The validation of the repaired datasets with \verb+val3dity+ has given a promising result and there was no new geometric or semantic error introduced during the healing. The algorithm can split the non-planar polygon and keeps the newly created polygons as few as possible. Errors due to floating point number precision or folds in the polygon can be healed in many ways. It is quite difficult is to find an efficient way to repair or heal these errors without any big impact in the original model. The following aspects are taken into account before choosing any repair method:

\begin{itemize}[leftmargin=*]
    \item Accuracy
    \item Computational efficiency
    \item Processing time
    \item Robustness
    \item Complexity        
\end{itemize}

\textbf{Problems regarding the repair of consecutive points:}\\
The algorithm checks for consecutive points if the number of vertices of a polygon is at least 5. If the polygon has only 3 vertices and the distance between two consecutive points are less than the NORMALS\_DEVIATION tolerance; therefore, the algorithm will not be able to find them. Dataset 3 has this error where the polygon contains only 3 points in which 2 points are consecutive. In this case, removing one of them will create GE\_R\_TOO\_FEW\_POINTS error.

\textbf{Problems regarding the identification of floating point number error:}\\
The floating point error is dependent to vertices of a face and the face can shares the a vertex with other adjacent faces. Several techniques have been experimented to find the most efficient way to identify floating point number error. In HDS, the half-edges are stored for each and every vertex of a face. The figure \ref{fig:hds-1} shows how exactly the half-edges of the vertices of a face are stored. \textbf{V} denotes the vertex and \textbf{h} denotes the halfedge. The distance between two vertices can be customized depending on the validation technique. For example, If the distance is 1 mm, then HDS will store both vertices separatly in which one of them has an error. The incidence relations of the vertices and faces can be used to find the correct vertex. The next section describes the possible approaches to identify the floating point number error.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{./files/hds.pdf}
	\caption[How the vertices of a polygon are stored in HDS]{How the vertices of a polygon are stored in HDS.}
	\label{fig:hds-1}
\end{figure}

\textbullet\ \textbf{Every vertex to all other vertices}\\
The distance between a vertex to all other vertices from all faces can be compared with the snapping tolerance. This approach is independent of other errors and can find the floating point number errors from the datasets. The dataset may contain gaps or overlaps between the geometries and this method is capable of finding the duplicate points due to floating point number errors. This approach may slow down the processing time if the dataset is very big. The implemented prototype uses this approach to identify the duplicate vertex.

\textbullet\ \textbf{Every vertex to the vertices from adjacent faces}\\
Unlike comparing to all vertices, the distance between a vertex and all vertices from adjacent faces can be compared. This approach does less computational effort and capable of finding duplicate vertex only for the correct face connectivity information. This approach first finds the incident face for a particular face and then finds all adjacent faces. This approach is useful when the geometry is a watertight object and the face connectivity is correct. The problem with this approach is that the geometric data structure might not be able to maintain the incident relations of particular vertex or face if they are not connected. The same problem occurs while finding the adjacency relation of a vertex (e.g. faces around a vertex) to repair floating point number error. The adjacency information of a geometrically disconnected vertex can show less adjacent faces. Thus the implemented algorithm compares each vertex to all other vertices to avoid such situations. 

\textbullet\ \textbf{Vertices around a particular vertex}\\
Connectivity information of a vertex can be used to find the adjacent vertices around a particular vertex. This approach uses the vertex connectivity relations and calculates the distance from each vertex to only adjacent vertices. For a large number of vertices, this approach could be more efficient than the other approaches if there is no gaps or overlaps between geometries. The correct connectivity between the geometries is a prerequisite for this approach. This approach is faster than the other approaches and capable of finding the duplicate points only if the dataset is free of connectivity error. 

The identification of floating point number error finds two vertices with the same probability. The next step is to find the connectivity relationship of both vertices to identify the correct vertex. The one with more adjacency or occurrence is considered more accurate. The one with less occurrence is considered as duplicate. The repair process explores the connectivity relation of both vertices and tries to find the correct vertex out of those two vertices. The section below describes the possible methods to repair duplicate vertices.

\textbf{Problems regarding the repair of floating point number error:}

\textbullet\ \textbf{Adjacency occurrence for both vertices}\\
The duplicate vertex can be replaced by the one with more occurrence in the adjacent faces. So, the vertex which is incident to more faces is considered as the correct vertex. If the occurrence is same for both vertices, any of them can be replaced by another one. The prototype uses this method and is capable of repairing the floating point number error. 

\textbullet\ \textbf{Intersection of three planes}\\
There might be another way to repair the duplicate vertex by calculating the intersection of at least three planes. The intersection of three adjacent faces can be calculated and the duplicate vertex is replaced by the point of intersection in all faces. This case, each vertex must be incident to at least three faces. In reality, datasets from heterogeneous sources can have vertex which has less than three incident faces. The accuracy of intersection point can be biased if the adjacent faces also have the error (e.g. planarity error). An iterative check can be performed to ensure that there is no new error created during the last processing. But the volume of the geometry can be changed at the end of the iterative process which is not desired. The 3D building in figure \ref{fig:vertex_3faces} (see red marks) shows the vertex incident to less than 3 faces situation.\\

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{./files/vertex_3faces.JPG}
    \caption{Vertex incident to less than three faces.}
    \label{fig:vertex_3faces}
\end{figure}

\textbf{Problems regarding the repair of planarity error:}

The algorithm can produce several triangular polygons depending on the location of vertices in the point sequence and the first vertex is common to all newly created polygons which ensures no overlaps or no self-intersections of polygons (figure \ref{fig:vertex_triangular_faces}). But the problem with this approach is the first vertex of all newly created polygons. For example, P1 is common in all newly created triangular polygons which increases the number of vertices. There might be another possible way to split the polygon by finding the largest deviation to the least-squares plane. The polygon split can be started from the vertex which has the largest deviation to the plane. But all vertices can have almost the same deviations to the plane if the plane-fitting is defined by the least-squares plane. This type of technique likely to produce intersections or holes between the newly created polygons if the non-planar polygon contains large number of points.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.1\textwidth]{./files/face_split_triangles.pdf}
	\caption{Triangular polygons created by the healing technique.}
	\label{fig:vertex_triangular_faces}
\end{figure}

The implementation can deal with the error situations, if there are duplicate vertices situated in different faces or more than one duplicate vertex in the same face. Processing of the decimal places for input and output, and during the validation and repair must be efficiently handled. The implemented prototype uses 15 decimal places with a possibility to extend. The implementation covers all aforementioned aspects and is capable of repairing them. The algorithm is tested with the imaginary data as well as the real dataset with the satisfactory output result. The validation and automatic repair are performed at the polygon level which covers the planarity error aspects addressed by the recent technical documentation, scientific papers, research works. The implementation uses CGAL library which provides many available functionalities to process the geometry; therefore, the implemented prototype can be easily extended which can be useful to repair other errors in the 3D building models.
