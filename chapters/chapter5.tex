\chapter{Prototype Implementation \label{cha:chapter5}}
The validation and repair methodology described in chapter \ref{cha:chapter4} was implemented in C++. The implemented prototype was tested with the real-world CityGML LOD2 3D building models. The chapter explains the implementation workflow and the features of the implemented prototype.

\section{Development Frameworks \label{sec:dev_struc}}
The implemented prototype uses Computational Geometric Algorithms Library\footnote{Computational Geometric Algorithms Library: https://www.cgal.org/} (CGAL) \citep{2017_cgal} to process the 3D building geometry. The CGAL library provides a lot of geometric functionality and mathematical operations which can be used during the processing of the 3D building geometry. The main motivations of using CGAL are:
\begin{itemize}[leftmargin=*]
    \item C++ implementation
    \item Robust
    \item Geometric data structures
    \item Geometric algorithms
\end{itemize}

CGAL has dependencies to other 3rd-party libraries like Boost, Standard Template Library (STL), Eigen, QT and etc. The Algorithm or platform-specific libraries can also be used which are required to perform some specific functionalities.

The implemented planarity error validation technique is similar to the \verb+val3dity+ \citep{2013_solid_validation} which also use the CGAL library as a part of their project. The repair methodology solely implemented by the combination of some CGAL functionalities and C++ programming. The FME Data Inspector was used to visualize and inspect the 3D building models.

\section{Workflow \label{sec:impl}}
The implementation is separated into two parts; (1) validation, (2) repair. The validation rules for all aspects of planarity error are checked in the validation section. If there are errors, then the repair algorithms for that specific errors are executed. Figure \ref{fig:impl_flowchart} shows the overview of the implemented workflow and the details are discussed in the next section.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{./files/impl_flowchart.pdf}
    \caption{Implementation workflow.}
    \label{fig:impl_flowchart}
\end{figure}

\subsection{Read CityGML Model \label{sec:read_citygml}}
The first step of the implementation is to read the CityGML 3D building models. As the non-planarity of polygon error exists on the {polygon} level and is independent to other adjacent {polygons}. So, polygons are the focus of interest in this case. The implementation can deal only with CityGML LOD2 3D building models in which the polygons do not contain any interior linear rings. Therefore, the algorithm considers an exterior linear ring as a polygon.

An initial check is performed to check the minimum requirements of the linear ring. For example, if the linear ring contains less than three points, no further processing for that linear ring and an error message is reported to the user. The id of a linear ring or a polygon can be useful to identify the non-planar polygons. If the polygon or linear ring does not contain an id, it is quite difficult to identify or address them. GML id of a polygon or a linear ring is a prerequisite for the implemented prototype. The algorithm uses polygon or linear ring id to recognize the particular non-planar polygon and during the generation of repaired CityGML models.

\subsection{CityGML to Polyhedral Surface \label{sec:store_hds}}
The 3D building geometry is initially stored into a polyhedral surface to navigate through the geometry using the incidence relationship. The CGAL provides different implementations of the polyhedral surfaces which are capable of storing 2-manifold or non-manifold objects. The CGAL \textbf{Polyhedron\_3} \citep{2017_cgal_Polyhedron} class only allows to store 2-manifold while non-manifold objects can be stored using CGAL \textbf{Nef\_polyhedron\_3} \citep{2017_cgal_Nef}. The CGAL \textbf{Surface\_mesh} is another way to store polyhedral surfaces which is similar to \textbf{Polyhedron\_3} class. The exteriors of polygons (linear rings) are stored as facets of the polyhedron. So, the number of polygons is equal to the number of facets in the polyhedron. The algorithm uses \textbf{CGAL::Surface\_mesh} to store and process 3D building geometry.

\subsection{Check Folds in the Polygon \label{sec:check_folds}}
The small folds can be easily detected by calculating the distance between two consecutive vertices within the same polygon. The implementation follows the same validation rule for checking small folds in the polygon which is explained in section \ref{sec:valid_rules1}. Figure \ref{fig:impl_flowchart_check1} shows the step by step workflow to check the folds within a polygon.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.60\textwidth]{./files/impl_flowchart_check1.pdf}
	\caption{Validation flowchart for checking small folds in the polygon.}
	\label{fig:impl_flowchart_check1}
\end{figure}

\subsection{Repair Folds in the Polygon \label{sec:rep_folds}}
After the identification of small folds in the polygon, the next step is to repair them. The algorithm simply removes one of those consecutive vertices which caused the fold. The repaired polygon has no folds but has the DISTANCE\_PLANE error. This error can be repaired by applying the polygon splitting algorithm. If there are consecutive same points in the polygon, the folds detection algorithm will be able to detect and remove one of the duplicate points.

\subsection{Check Floating Point Number Error \label{sec:preprocessing}}
The preprocessing should be performed on the raw input dataset. The first step of this process is to check whether any rounding errors are present in the dataset. Floating pointing number error should be repaired before the validation of planarity. This process is used to identify the duplicate points which are occurred due to the floating point numbers in the point coordinates.

Snapping tolerance $(\epsilon_3)$ is defined at this stage of processing to find the floating point number errors in the dataset. This tolerance value (e.g. 0.01 mm) is the distance between an incident vertex and all vertices from adjacent faces. The definition of $\epsilon_3$ depends on the requirements of the intended application of use and quality of the input or output model. The figure \ref{fig:impl_flowchart_check2} shows how the suggested validation for rounding error is performed.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.55\textwidth]{./files/impl_flowchart_check2.pdf}
    \caption{Validation flowchart for checking rounding error.}
    \label{fig:impl_flowchart_check2}
\end{figure}

\subsection{Repair Floating Point Number Error \label{sec:rep_flt_err}}
If there is any floating point number error in the dataset, it should be repaired at this stage of processing. The suggested repair technique explained in section \ref{sec:rep_flt} was implemented and the following figure \ref{fig:impl_flowchart_repair1} shows a step by step implementation repair workflow. The repair approach only considers two vertices from adjacent faces and finds the erroneous vertex depending on their adjacency information.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.80\textwidth]{./files/impl_flowchart_repair1.pdf}
    \caption{Repair flowchart for floating point number error.}
    \label{fig:impl_flowchart_repair1}
\end{figure}

\subsection{Planarity Check \label{sec:check_val}}
This process defines the tolerance  $(\epsilon_1)$ to validate all facets (e.g. polygons) of the polyhedral object whether they are planar or not. The plane is defined by the least-squares 3D plane-fitting algorithm which uses CGAL linear least-squares plane-fitting function (CGAL::linear\_least\_square\_fitting\_3()). The distance between two vertices or from a point to the plane can be calculated by using CGAL::squared\_distance() function. As explained in section \ref{sec:planarity}, the validation process considers a polygon as invalid (not a particular point) depending on the $(\epsilon_1)$ tolerance. Figure \ref{fig:impl_flowchart_check3} is the workflow which is based on the suggested validation rules.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.70\textwidth]{./files/impl_flowchart_check3.pdf}
    \caption{Planarity check workflow.}
    \label{fig:impl_flowchart_check3}
\end{figure}

\subsection{Healing of Planarity Error \label{sec:healing_err}}
The healing methodology is explained in section \ref{sec:ps_algo}. As soon as the validation is completed, the next step is to heal the planarity error (up to a certain accuracy). The output accuracy of the repaired model can be customized by defining a repair tolerance limit (e.g. 0.0001 mm). If the newly created polygon contains only three points, then the accuracy is 100 \% because three points defining a plane is always planar. On the other hand, if the new polygon contains more than 3 points, the tolerance value is used to decide whether the point belongs to the plane or not. 

For example, the repaired datasets can be used as an input to an analysis application and an accuracy of 0.0001 mm for non-planarity of polygons are required. In this case, the above-mentioned repair tolerance value is 0.0001 mm by which the algorithm splits the polygon so that the datasets do not have any polygons where $\epsilon_1$ is less than 0.0001 mm. 

The new polygons must be created according to the ISO 19107 spatial schema and CityGML standard rules. Otherwise, the new errors like self-intersections, the wrong orientation of the polygon can occur which is not desired. The detailed workflow for splitting the non-planar polygon is depicted in figure \ref{fig:impl_flowchart_repair2} below:

\begin{figure}[h]
    \centering
    \includegraphics[width=0.90\textwidth]{./files/impl_flowchart_repair2.pdf}
    \caption{Overview of polygon splitting algorithm.}
    \label{fig:impl_flowchart_repair2}
\end{figure}

\subsection{Generate Planarized CityGML Model \label{sec:write_citygml}}
After repairing of planarity error, the creation of repaired CityGML model is done at this stage. The repaired 3D building is generated with all of its features, textures, attributes and etc. The repaired non-planar polygons are written as multiple polygons but semantic information remains the same.

\subsection{Test Report \label{sec:error_report}}
The implemented algorithm returns a detailed test report to the user. The test report can be divided into two parts, error report and repair report. \\
\textbf{Error report:} This error report contains the information and error reports of the validation. The error report can be useful to know more about the planarity error and the dataset such as:
\begin{itemize}[leftmargin=*]
    \item Defined tolerances
    \item Total number of polygons
    \item Number of floating point error
    \item Number of non-planar polygons   
\end{itemize}

\textbf{Repair report:} This report contains similar to error report but with more information on the repair process. The repair report can have following results:
\begin{itemize}[leftmargin=*]
    \item Repair tolerance
    \item Number of repaired floating point error
    \item Number of repaired non-planar polygons
    \item GML id of the non-planar polygon
    \item Number of vertices before repair
    \item Number of split polygons after healing
    \item Number of vertices of a split polygon
    \item Total number of repaired polygons
    \item Repair status 
\end{itemize}