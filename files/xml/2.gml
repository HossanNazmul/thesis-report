<bldg:lod2MultiSurface>
	<gml:MultiSurface>
		<gml:surfaceMember>
			<gml:Polygon gml:id="GML_3811-4796-a92f-3fd037c8e668">
				<gml:exterior>
					<gml:LinearRing>
						<gml:posList srsDimension="3">458885 5438350 112 458885.010000000009313 5438355 112 458885 5438355 115 458885 5438350 112</gml:posList>
					</gml:LinearRing>
				</gml:exterior>
			</gml:Polygon>
		</gml:surfaceMember>
	</gml:MultiSurface>
</bldg:lod2MultiSurface>	
<bldg:lod2MultiSurface>
	<gml:MultiSurface>
		<gml:surfaceMember>
			<gml:Polygon gml:id="_planarized_face_0">
				<gml:exterior>
					<gml:LinearRing>
						<gml:posList srsDimension="3">458885 5438350 112 458885 5438355 115 458885 5438352.5 117 458885 5438350 115 458885 5438350 112</gml:posList>
					</gml:LinearRing>
				</gml:exterior>
			</gml:Polygon>
		</gml:surfaceMember>
	</gml:MultiSurface>
</bldg:lod2MultiSurface>